# Helm push Docker Image - deprecated

# This plugin has been moved to [helm-kubectl](https://gitlab.com/applifting-cloud-engineering/images/helm-kubectl) image

This repo serves the purpose of creating Docker images to be used in CI/CD pipelines.
The CI/CD job in this project is scheduled to run weekly and build a new Docker image with the latest tool's version while tagging the image `latest`.
The image is also tagged with `{HELM_VERSION}`.

### You can find all the previous versions in the [Container Registry](https://gitlab.com/applifting-cloud-engineering/images/helm-push/container_registry/3414327).

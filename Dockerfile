ARG HELM_VERSION

FROM alpine/helm:${HELM_VERSION}

RUN helm plugin install https://github.com/chartmuseum/helm-push.git
